function validate(myform) 
{
	var fn=document.myform.fn.value;
	var ln=document.myform.ln.value;
	var month=document.myform.m.value;
	var date=document.myform.d.value;
	var year=document.myform.y.value;
	var un=document.myform.un.value;
	var pwd=document.myform.pwd.value;
	var cpwd=document.myform.cpwd.value;
	var Qual=document.myform.Qual.value;
	var add=document.myform.add.value;
	var e=document.myform.e.value;
	var pn=document.myform.pn.value;
	var h=document.myform.h.value;
	var name=/^([a-zA-Z]*)$/;
	var space=/\s/;
	var uname=/^([a-zA-Z]{1}[a-zA-Z0-9._]*[a-zA-Z0-9])$/;
   var splchar=/[!@#$%^&*+-><?:]/;
	var mbno=/^([7-9]{1}[0-9]{9})$/;
	var checkemail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
	if(fn == "") {
		 inlineMsg('fn','You must enter your name.',2);
		 return false;
	}
	if(fn.length<6)
	{
		inlineMsg('fn','u must not have lessthan 6 characters',2);
		return false;
	}
	if(fn.match(space))
	{
		inlineMsg('fn','u must not have spaces',2);
		return false;
	}
	if(!fn.match(name))
	{
		inlineMsg('fn','u must enter alphabets ',2);
		return false;
	}
	if(ln=="")
   {
		inlineMsg('ln','u must enter lastname',2);
		 return false;
   }
	if(ln.match(space))
	{
	inlineMsg('ln','u must not have spaces',2);
	return false;
	}
	if(!ln.match(name))
	{
		inlineMsg('ln','u must enter alphabets',2);
		return false;
	}
	if(month == "month") {
		 inlineMsg('m','You must select Month.',2);
		 return false;
	}
	if(date == "date") {
		 inlineMsg('d','You must select Date.',2);
		 return false;
	}
	if(year == "year") {
		 inlineMsg('y','You must select Year.',2);
		 return false;
	}
	if(un=="")
	{
		inlineMsg('un','u must enter uname.',5);
		return false;
	}
	if(un.length<6)
	{
		inlineMsg('un','u must not have lessthan 6 characters',2);
		return false;
	}
	if(un.match(splchar))
	{
		inlineMsg('un','u must not have specialcharacters',3);
		return false;
	}	
	if(!un.match(uname))
	{
		inlineMsg('un','umust enter valid name',2);
		return false;
	}
	if(pwd=="")
	{
		inlineMsg('pwd','u must enter password.',3);
		return false;
	}
	if(!pwd.match(splchar))
	{
		inlineMsg('pwd','u must  have atleast one specialcharacter',3);
		return false;
	}	
	if(cpwd=="")
	{
		inlineMsg('cpwd','u must enter conform password.',3);
		return false;
	}
	if(!cpwd.match(splchar))
	{
		inlineMsg('cpwd','u must  have atleast one specialcharacter',3);
		return false;
	}	
	if(cpwd!=pwd)
	{
		inlineMsg('cpwd','u must enter correct password.',2);
		return false;
	}
	if(Qual=="select")
	{
		inlineMsg('Qual','u must select any one.',3);
		return false;
	}
	if((WholeSaler=="")||(Farmer==""))
	{
		inlineMsg('Farmer','u must check any one.',3);
		return false;
	}
	if(add=="")
	{
		inlineMsg('add','u must enter address.',2);
		return false;
	}
	if(e=="")
	{
		inlineMsg('e','u must enter email address.',2);
		return false;
	}
	if(!e.match(checkemail))
	{
		inlineMsg('e','u must enter valid email address.',2);
		return false;
	}
	if(pn=="")
	{
		inlineMsg('pn','u must enter mobileno',2);
		return false;
	}
	if(!pn.match(mbno))
	{	
		inlineMsg('pn','u must enter valid mobileno',2);
		return false;
	}
	return true;
}


// START OF MESSAGE SCRIPT //

var MSGTIMER = 20;
var MSGSPEED = 5;
var MSGOFFSET = 3;
var MSGHIDE = 3;

// build out the divs, set attributes and call the fade function //
function inlineMsg(target,string,autohide) {
  var msg;
  var msgcontent;
  if(!document.getElementById('msg')) {
    msg = document.createElement('div');
    msg.id = 'msg';
    msgcontent = document.createElement('div');
    msgcontent.id = 'msgcontent';
    document.body.appendChild(msg);
    msg.appendChild(msgcontent);
    msg.style.filter = 'alpha(opacity=0)';
    msg.style.opacity = 0;
    msg.alpha = 0;
  } else {
    msg = document.getElementById('msg');
    msgcontent = document.getElementById('msgcontent');
  }
  msgcontent.innerHTML = string;
  msg.style.display = 'block';
  var msgheight = msg.offsetHeight;
  var targetdiv = document.getElementById(target);
  targetdiv.focus();
  var targetheight = targetdiv.offsetHeight;
  var targetwidth = targetdiv.offsetWidth;
  var topposition = topPosition(targetdiv) - ((msgheight - targetheight) / 2);
  var leftposition = leftPosition(targetdiv) + targetwidth + MSGOFFSET;
  msg.style.top = topposition + 'px';
  msg.style.left = leftposition + 'px';
  clearInterval(msg.timer);
  msg.timer = setInterval("fadeMsg(1)", MSGTIMER);
  if(!autohide) {
    autohide = MSGHIDE;  
  }
  window.setTimeout("hideMsg()", (autohide * 1000));
}

// hide the form alert //
function hideMsg(msg) {
  var msg = document.getElementById('msg');
  if(!msg.timer) {
    msg.timer = setInterval("fadeMsg(0)", MSGTIMER);
  }
}

// face the message box //
function fadeMsg(flag) {
  if(flag == null) {
    flag = 1;
  }
  var msg = document.getElementById('msg');
  var value;
  if(flag == 1) {
    value = msg.alpha + MSGSPEED;
  } else {
    value = msg.alpha - MSGSPEED;
  }
  msg.alpha = value;
  msg.style.opacity = (value / 100);
  msg.style.filter = 'alpha(opacity=' + value + ')';
  if(value >= 99) {
    clearInterval(msg.timer);
    msg.timer = null;
  } else if(value <= 1) {
    msg.style.display = "none";
    clearInterval(msg.timer);
  }
}

// calculate the position of the element in relation to the left of the browser //
function leftPosition(target) {
  var left = 0;
  if(target.offsetParent) {
    while(1) {
      left += target.offsetLeft;
      if(!target.offsetParent) {
        break;
      }
      target = target.offsetParent;
    }
  } else if(target.x) {
    left += target.x;
  }
  return left;
}

// calculate the position of the element in relation to the top of the browser window //
function topPosition(target) {
  var top = 0;
  if(target.offsetParent) {
    while(1) {
      top += target.offsetTop;
      if(!target.offsetParent) {
        break;
      }
      target = target.offsetParent;
    }
  } else if(target.y) {
    top += target.y;
  }
  return top;
}

// preload the arrow //
if(document.images) {
  arrow = new Image(7,80); 
  arrow.src = "/images/msg_arrow.gif"; 
}