<%@page import="java.sql.*" %>
<%@page import="java.util.*" %>
<%@ page import="com.vcs.kbcss.MyDriver"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>::KBCSS::</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="mainbg">
<!-- header begins -->
<%String uname=(String)session.getAttribute("uname");
String userid=(String)session.getAttribute("userid");
//int uid=Integer.parseInt(userid);


%>

<div id="header">
	<div id="buttons">
		<div class="menu_width">
		<ul>
			   
			<li><a class="active">Welcome&nbsp;&nbsp;<%=uname%></a></li>

		
	
			
			
			<li><a href="logout.jsp" title="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a></li>
		</ul>
		</div>
	</div>
	<div id="logo">
		<a href="#">Knowledge Based Community Sharing System</a>
		<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--Interaction With Industry</h5>
		
	</div>
<!-- header ends -->
<!-- content begins -->
<div id="main">
<div id="main_top"></div>
<div id="content_bg">
  <div id="content"></div>
  <div id="right"> </br>
    <h2>My Account</h2>
    <div class="categories">
       <ul>
        <li><a href="articles.jsp">Articles</a></li>        
        <li><a href="mat.jsp">Materials</a></li>
        <li><a href="res.jsp">Research papers</a></li>
        <li><a href="postma.jsp">Post Material/Research papers</a></li>
        <li><a href="reports.jsp">Reports</a></li>
        <li><a href="discForum.jsp">DiscussionForum</a></li> 
        <li><a href="cp.jsp">Change Password</a></li>
      </ul>
    </div>
  </div>
  <div class="rrr">
     <div class="TabbedPanelsContentGroup">
       <div class="TabbedPanelsContent">
         <table width="100%" cellpadding="4" cellspacing="3" >
       
           
           <%String title=request.getParameter("title");
		   String artid=request.getParameter("aid");%>           
           <tr>
             <td align="left" valign="top">&nbsp;</td>
             <td height="83" align="left" valign="top">Comment on <strong><%=title%></strong></td>
             <td align="left" valign="top">&nbsp;</td>
             <td align="left" valign="top">&nbsp;</td>
           </tr>
           <form name="post" action="insertComment.jsp" method="post">          
             
           
          
           <tr>
             
             <td height="27" align="left" valign="top"><strong>Comment :</strong></td>             
             <td align="left" valign="top"><div align="left">
               <textarea name="comment" id="content" required></textarea>
             </td>
           </tr>
           <tr>
             <td align="left" valign="top"><input type="hidden" name="aid" value="<%=artid%>" /></td>
             <td align="left" valign="top" >
              
                 <input type="submit" value="Post" />
               </td>
            
           </tr></form>
   
         </table>
         <center>
         <%
		 String message=request.getParameter("message");
		   if(message!=null)
		   {%>
             <strong><%=message%></strong>
			<%   }
		 %></center>
       </div>
     </div>
   </div>

</div>

</div>
</div>   
<!-- content ends -->
<!-- footer begins -->
<div id="footer">
  <jsp:include page="footer.jsp" />
<!-- footer ends -->

</div>
</div>
</body>
</html>
