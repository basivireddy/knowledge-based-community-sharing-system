<%@page import="java.sql.*" %>
<%@ page import="com.vcs.kbcss.MyDriver"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Knowledge Sharing Community</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="mainbg">
<!-- header begins -->
<%String uname=(String)session.getAttribute("uname");
String userid=(String)session.getAttribute("userid");
//int uid=Integer.parseInt(userid);


%>

<div id="header">
	<div id="buttons">
		<div class="menu_width">
		<ul>
			   
			<li><a class="active">Welcome&nbsp;&nbsp;<%=uname%></a></li>

		
	
			
			
			<li><a href="logout.jsp" title="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a></li>
		</ul>
		</div>
	</div>
	<div id="logo">
		<a href="#">Knowledge Based Community Sharing System</a>
		<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--Interaction With Industry</h5>
		
	</div>
<!-- header ends -->
<!-- content begins -->
<div id="main">
<div id="main_top"></div>
<div id="content_bg">
  <div id="content"></div>
  <div id="right"> </br>
    <h2>My Account</h2>
    <div class="categories">
      <ul>
        <li><a href="articles.jsp">Articles</a></li>      
        <li><a href="mat.jsp">Materials</a></li>
        <li><a href="res.jsp">Research papers</a></li>
        <li><a href="interests.jsp">Interests</a></li> 
        <li><a href="users.jsp">Users</a></li>
        <li><a href="discForum.jsp">DiscussionForum</a></li> 
          <li><a href="reports.jsp">Reports</a></li>        
      </ul>
    </div>
  </div>
  <div class="rrr">
    <div class="spaa">
     
     <table width="97%" cellpadding="4" align="right">
         <tr>
          <td align="left" valign="top"><strong>Materials/Research Papers Report</strong></td>
          <td align="center" ><a href="likeReportMa.jsp" title="Like Reports Chart"><font color="#FFFFFF"><strong><img src="images/barchart.png" alt="Barchart" width="30" height="30" /><img src="images/likeb.png" alt="Like" width="30" height="28" /></strong></font></a></td>
          <td align="left"><a href="unlikeReportMa.jsp" title="Dislikes Report Chart"><font color="#FFFFFF"><strong><img src="images/barchart.png" alt="Barchart" width="30" height="30" /><img src="images/dis.png" alt="Dislike" width="30" height="28" /></strong></font></a></td>
        </tr>
        <tr align="center">
          <td ><strong>Post Name</strong></td>  
          <td ><strong>Post Type</strong></td>        
          <td ><strong>Like</strong></td>
          <td ><strong>Unlike</strong></td>
          
        </tr>
         <%
        Statement st12=MyDriver.getConnection("READ");
		ResultSet rs12=st12.executeQuery("select * from postmrp");
		while(rs12.next())
		{
							  %>
							
        <tr align="center">
         <td ><%=rs12.getString("title")%></td>
         	<td><%if(rs12.getString("mrp").equals("m"))
            {
            out.println("Material");
            }
            if(rs12.getString("mrp").equals("rp"))
            {
            out.println("Research Paper");
            }%></td>
          <td ><%=rs12.getString("like1")%></td> 
          <td ><%=rs12.getString("unlike")%></td>           </tr>
  <%}%>
        
        <tr align="center">
         <td >&nbsp;</td>
          <td >&nbsp;</td> 
          <td >&nbsp;</td>          
        </tr>
        <tr align="center">
         <td >&nbsp;</td>
          <td >&nbsp;</td> 
          <td >&nbsp;</td>          
        </tr>
      </table>
      
      
    </div>
  </div>
</div>

</div>
</div>   
<!-- content ends -->
<!-- footer begins -->
<div id="footer">
   <jsp:include page="footer.jsp" />
</div>
</div>
</body>
</html>
