<%@page import="java.sql.*" %>
<%@ page import="com.vcs.kbcss.MyDriver"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Knowledge Sharing Community</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<script type="text/javascript">
		
			/**
			 * Visualize an HTML table using Highcharts. The top (horizontal) header 
			 * is used for series names, and the left (vertical) header is used 
			 * for category names. This function is based on jQuery.
			 * @param {Object} table The reference to the HTML table to visualize
			 * @param {Object} options Highcharts options
			 */
			Highcharts.visualize = function(table, options) {
				// the categories
				options.xAxis.categories = [];
				$('tbody th', table).each( function(i) {
					options.xAxis.categories.push(this.innerHTML);
				});
				
				// the data series
				options.series = [];
				$('tr', table).each( function(i) {
					var tr = this;
					$('th, td', tr).each( function(j) {
						if (j > 0) { // skip first column
							if (i == 0) { // get the name and init the series
								options.series[j - 1] = { 
									name: this.innerHTML,
									data: []
								};
							} else { // add values
								options.series[j - 1].data.push(parseFloat(this.innerHTML));
							}
						}
					});
				});
				
				var chart = new Highcharts.Chart(options);
			}
				
			// On document ready, call visualize on the datatable.
			$(document).ready(function() {			
				var table = document.getElementById('datatable'),
				options = {
					   chart: {
					      renderTo: 'container',
					      defaultSeriesType: 'column'
					   },
					   title: {
					      text: 'Report of Material/Research Paper (Likes)'
					   },
					   xAxis: {
					   },
					   yAxis: {
					      title: {
					         text: 'Likes'
					      }
					   },
					   tooltip: {
					      formatter: function() {
					         return '<b>'+ this.series.name +'</b><br/>'+
					            this.y +' '+ this.x.toLowerCase();
					      }
					   }
					};
				
			      					
				Highcharts.visualize(table, options);
			});
				
		</script>
		
</head>
<body>
<div id="mainbg">
<!-- header begins -->
<%String uname=(String)session.getAttribute("uname");
String userid=(String)session.getAttribute("userid");
//int uid=Integer.parseInt(userid);


%>

<div id="header">
	<div id="buttons">
		<div class="menu_width">
		<ul>
			   
			<li><a class="active">Welcome&nbsp;&nbsp;<%=uname%></a></li>
			<li><a href="logout.jsp" title="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a></li>
		</ul>
		</div>
	</div>
	<div id="logo">
		<a href="#">Knowledge Based Community Sharing System</a>
		<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--Interaction With Industry</h5>
		
	</div>
<!-- header ends -->
<!-- content begins -->
<div id="main">
<div id="main_top"></div>
<div id="content_bg">
  <div id="content"></div>
  <div id="right"> </br>
    <h2>My Account</h2>
    <div class="categories">
      <ul>
        <li><a href="articles.jsp">Articles</a></li>      
        <li><a href="mat.jsp">Materials</a></li>
        <li><a href="res.jsp">Research papers</a></li>
        <li><a href="interests.jsp">Interests</a></li> 
        <li><a href="users.jsp">Users</a></li>
        <li><a href="discForum.jsp">DiscussionForum</a></li> 
          <li><a href="reports.jsp">Reports</a></li>        
      </ul>
    </div>
  </div>
  <div class="rrr">
    <div class="spaa">
    <div id="container" style="width: 550px; height: 400px; margin: 0 auto"></div>
		
		
		<table id="datatable" border="1">
       
			<thead>
				<tr align="left">
					<th></th>
					 <%
        Statement st12=MyDriver.getConnection("READ");
		ResultSet rs12=st12.executeQuery("select * from postmrp");
		while(rs12.next())
		{ 
		String title=rs12.getString("title");%><th><%=rs12.getString("title")%></th> <%}%>			
					
				</tr>
			</thead>
           
			<tbody>
            
				<tr align="center">
					<th>Likes</th>
                    <%Statement st1=MyDriver.getConnection("READ");
		ResultSet rs1=st1.executeQuery("select like1 from postmrp");
		while(rs1.next())
		{ %>
					<td><%=rs1.getString("like1")%></td> <%}%>
					
				</tr> 				
				
				
			</tbody>
		</table>
    
    </div>
  </div>
</div>

</div>
</div>   
<!-- content ends -->
<!-- footer begins -->
<div id="footer">
  <jsp:include page="footer.jsp" /><!-- footer ends -->

</div>
</div>
</body>
</html>
