<%@page import="java.sql.*" %>

<%@ page import="com.vcs.kbcss.MyDriver"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Knowledge Share Community</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="mainbg">
<!-- header begins -->
<%String uname=(String)session.getAttribute("uname");
String userid=(String)session.getAttribute("userid");
//int uid=Integer.parseInt(userid);


%>

<div id="header">
	<div id="buttons">
		<div class="menu_width">
		<ul>
			   
			<li><a class="active">Welcome&nbsp;&nbsp;<%=uname%></a></li>

		
	
			
			
			<li><a href="logout.jsp" title="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a></li>
		</ul>
		</div>
	</div>
	<div id="logo">
		<a href="#">Knowledge Based Community Sharing System</a>
		<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--Interaction With Industry</h5>
		
	</div>
<!-- header ends -->
<!-- content begins -->
<div id="main">
<div id="main_top"></div>
<div id="content_bg">
  <div id="content"></div>
  <div id="right"> </br>
    <h2>My Account</h2>
    <div class="categories">
    <ul>
        <li><a href="articles.jsp">Articles</a></li>      
        <li><a href="mat.jsp">Materials</a></li>
        <li><a href="res.jsp">Research papers</a></li>
        <li><a href="interests.jsp">Interests</a></li> 
        <li><a href="users.jsp">Users</a></li>
        <li><a href="discForum.jsp">DiscussionForum</a></li> 
          <li><a href="reports.jsp">Reports</a></li>        
      </ul>
    </div>
  </div>
  <div class="rrr">
    <div class="spaa">
      <%
						 
						 
						 
						 
						String name="";
						 String date="";
							Statement st12=MyDriver.getConnection("READ");
						 ResultSet rs12=st12.executeQuery("select * from articles where field='ece'");
						while(rs12.next())
						  {
						 name=rs12.getString("title");	
						 date=rs12.getString("postedon");
						  
			  
			  %>
      <table width="97%" cellpadding="4"  >
        <tr>
          <td align="left" valign="top">&nbsp;</td>
          <td height="83" align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left" valign="top"><strong>Article Posted By :</strong></td>
          <td align="left" valign="top"><div align="right">
            <div class="iiii">
              <div align="left">Kumar</div>
            </div>
            <div class="iiiii"><strong>Posted On :</strong><%=date%></div>
          </div></td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
          <td height="149" align="left" valign="top"><div class="dataartcile">
            <p><strong><%=name%></strong></p>
          </div></td>
          <td align="left" valign="top"><div class="dataartcile">
            <p><strong><%=rs12.getString("content")%></strong></p>
          </div></td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
          <td height="81" align="left" valign="top" colspan="2"><table width="460" border="0">
            <tr>
              <td><img src="images/likeb.png" alt="like" width="24" height="21" /><strong> <%=rs12.getString("like1")%></strong></td>
              <td><img src="images/dis.png".jpg" alt="like" width="24" height="21" /><strong> <%=rs12.getString("unlike")%></strong></td>
              <td>&nbsp;</td>
              
            </tr>
          </table></td>
        </tr>
      </table>
      <%}%>
    </div>
  </div>
</div>

</div>
</div>   
<!-- content ends -->
<!-- footer begins -->
<div id="footer">
   <jsp:include page="footer.jsp" /><!-- footer ends -->

</div>
</div>
</body>
</html>
