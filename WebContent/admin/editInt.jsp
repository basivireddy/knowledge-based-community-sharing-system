<%@page import="java.sql.*" %>
<%@ page import="com.vcs.kbcss.MyDriver"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Knowledge Sharing Community</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="mainbg">
<!-- header begins -->
<%String uname=(String)session.getAttribute("uname");
String userid=(String)session.getAttribute("userid");
//int uid=Integer.parseInt(userid);


%>

<div id="header">
	<div id="buttons">
		<div class="menu_width">
		<ul>
			   
			<li><a class="active">Welcome&nbsp;&nbsp;<%=uname%></a></li>
			<li><a href="logout.jsp" title="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a></li>
		</ul>
		</div>
	</div>
	<div id="logo">
		<a href="#">Knowledge Based Community Sharing System</a>
		<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--Interaction With Industry</h5>
		
	</div>
<!-- header ends -->
<!-- content begins -->
<div id="main">
<div id="main_top"></div>
<div id="content_bg">
  <div id="content"></div>
  <div id="right"> </br>
    <h2>My Account</h2>
    <div class="categories">
      <ul>
        <li><a href="articles.jsp">Articles</a></li>      
        <li><a href="mat.jsp">Materials</a></li>
        <li><a href="res.jsp">Research papers</a></li>
        <li><a href="interests.jsp">Interests</a></li> 
        <li><a href="users.jsp">Users</a></li>
        <li><a href="discForum.jsp">DiscussionForum</a></li> 
          <li><a href="reports.jsp">Reports</a></li>        
      </ul>
    </div>
  </div>
  <div class="rrr">
    <div class="spaa">
    <%
	Statement st=null;
	ResultSet rs=null;
	String fname=null;
	String fdesc=null;
	String fid=request.getParameter("fid");
	st=MyDriver.getConnection("READ");
	rs=st.executeQuery("select *from fields where fid='"+fid+"'");
	if(rs.next())
	{
		fname=rs.getString("fname");
		fdesc=rs.getString("fdesc");
		
	}
	%>
     <form name="interests" action="updateInt.jsp" method="post">
      <table width="97%" cellpadding="4" align="right" >
         <tr>
          <td align="left" valign="top"><strong>Edit / Update Interest</strong></td>
          <td height="81" align="left" valign="top" colspan="2"><input type="hidden" name="fid" value="<%=fid%>" /></td>
        </tr>
        <tr>
          <td align="right">Field : </td>          
          <td ><input type="text" name="field" value="<%=fname%>"/></td>
        </tr>
        <tr>
         <td align="right">Desc : </td>
          <td ><textarea name="fielddesc" ><%=fdesc%></textarea></td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
          <td align="left"><input type="submit" name="Update" value="Update" />&nbsp;&nbsp;&nbsp;&nbsp;<a href="interests.jsp"><input type="button" name="Cancel" value="Cancel" /></a></td>
        </tr>
      </table>
      </form>
    </div>
  </div>
</div>

</div>
</div>   
<!-- content ends -->
<!-- footer begins -->
<div id="footer">
    <jsp:include page="footer.jsp" /><!-- footer ends -->

</div>
</div>
</body>
</html>
