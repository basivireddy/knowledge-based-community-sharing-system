<%@page import="java.sql.*" %>

<%@ page import="com.vcs.kbcss.MyDriver"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Knowledge Sharing Community</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="mainbg">
<!-- header begins -->
<%String uname=(String)session.getAttribute("uname");
String userid=(String)session.getAttribute("userid");;
//int uid=Integer.parseInt(userid);


%>

<div id="header">
	<div id="buttons">
		<div class="menu_width">
		<ul>
			   
			<li><a class="active">Welcome&nbsp;&nbsp;<%=uname%></a></li>

		
	
			
			
			<li><a href="logout.jsp" title="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a></li>
		</ul>
		</div>
	</div>
	<div id="logo">
		<a href="#">Knowledge Based Community Sharing System</a>
		<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--Interaction With Industry</h5>
		
	</div>
<!-- header ends -->
<!-- content begins -->
<div id="main">
<div id="main_top"></div>
<div id="content_bg">
  <div id="content"></div>
  <div id="right"> </br>
    <h2>My Account</h2>
    <div class="categories">
      <ul>
        <li><a href="articles.jsp">Articles</a></li>
        <li></li>
        <li><a href="mat.jsp">Materials</a></li>
        <li><a href="res.jsp">Research papers</a></li>
        <li><a href="postma.jsp">Post Material/Research papers</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="discForum.jsp">DiscussionForum</a></li> 
        <li><a href="cp.jsp">Change Password</a></li>
      </ul>
    </div>
  </div>
  <div class="rrr">
     <div class="TabbedPanelsContentGroup">
       <div class="TabbedPanelsContent">
         <table width="100%" cellpadding="4" cellspacing="3" >
       
           <tr>
             <td width="26%" align="left" valign="top">&nbsp;</td>
             <td width="62%" height="83" align="left" valign="top">&nbsp;</td>
             </tr><form name="post" action="changep.jsp" method="post">
           <tr>
             <td align="left" valign="top">&nbsp;</td>
             <td height="114" align="left" valign="top"><table width="319" border="0">
               <tr>
                 <td width="140" height="35">Current Password</td>
                 <td width="169"><input type="password" name="opswd" required/></td>
               </tr>
               <tr>
                 <td height="33">New Password</td>
                 <td><input name="opswd2" type="password" id="opswd2" required /></td>
               </tr>
               <tr>
                 <td height="35">Confirm password</td>
                 <td><input type="password" name="opswd3" required/></td>
               </tr>
               <tr>
                 <td></td>
                 <td><input type="submit" value="Change" /></td>
               </tr>
             </table>
             <td align="left" valign="top">&nbsp;</td>
           </tr>
           <tr>
             <td align="left" valign="top">&nbsp;</td>
             <td height="81" align="left" valign="top">&nbsp;</td>
            
           </tr></form>
   
         </table>
       </div>
     </div>
   </div>

</div>

</div>
</div>   
<!-- content ends -->
<!-- footer begins -->
<div id="footer">
 <jsp:include page="footer.jsp" /> 
<!-- footer ends -->

</div>
</div>
</body>
</html>
