package com.vcs.kbcss;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

 public class MyDriver
{
	static {try{Class.forName("com.mysql.jdbc.Driver").newInstance();}catch(Exception e){e.printStackTrace();}	}
	 private static Connection con;
  private static Statement st;
  private static PreparedStatement pst;
 private static ResultSet rs; 

    public MyDriver()
    {
    }

	public Connection getCon()throws Exception{
		 if(con == null||con.isClosed())
        	con=DriverManager.getConnection("jdbc:mysql://localhost:3306/kbcss","root","root");
		 else return con;
			return con;
		}

    public static Statement getConnection(String st1)
        throws Exception
    {     
			con=new MyDriver().getCon();
       if(st1.equals("READ")){
		        st=con.createStatement();
				}
		else
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
        return st;
    }

  public static PreparedStatement getPrepare(String st) throws Exception
  {
	 con=new MyDriver().getCon();
	  pst=con.prepareStatement(st);
	  return pst;
	     
  }
  }